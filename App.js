import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import ToDoInput from './components/TodoInput';
import TodoItem from './components/TodoItem';
import Header from './components/Header'

export default function App() {
  const [todoItems, setTodoItems] = useState([]);

  const addItemHandler = (itemValue) => {
    if(itemValue.length === 0) {
      return;
    }
    setTodoItems(currentItems => [
      ...currentItems,
      {id: Math.random().toString(), value: itemValue}
    ]);
  };

  const removeItemHandler = (ItemId) => {
    setTodoItems(currentItems => {
      return currentItems.filter((item) => item.id !== ItemId);
    });
  };

  return (
    <View style={styles.container}>
      <Header title="TODO List" />
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={todoItems}
        renderItem={itemdata =>
          <TodoItem
            id={itemdata.item.id}
            onDelete={removeItemHandler}
            title={itemdata.item.value}
          />} 
      />
      <View style={styles.inputContainer}>
        <ToDoInput onAddItem={addItemHandler} /> 
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textContainer: {
    backgroundColor: 'blue',
    alignItems: 'center',
    height: 50,
  },
  text: {
    color: 'white',
    fontSize: 30
  },
  inputContainer: {
    width: '100%',
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    zIndex: 1
  }
});
