import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, ScrollView, KeyboardAvoidingView } from 'react-native';

const TodoInput = props => {
   const [enterInput, setEnterInput] = useState('');

   const goalInputHandler = (enteredText) => {
    setEnterInput(enteredText);
   };

   const addGoalHandler = () => {
    props.onAddItem(enterInput);
    setEnterInput('');
   }

   return (
    <ScrollView>
      <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={30}>
        <View style={styles.inputContainer}>
            <TextInput
                placeholder="Enter Item"
                style={styles.input}
                onChangeText={goalInputHandler}
                value={enterInput}
            />
            <View style={styles.button}><Button title=" ADD " onPress={addGoalHandler} /></View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
    inputContainer: {
      flexDirection: 'row',
    },
    input: {
      flex: 2,
      padding: 9,
      margin: 10,
      borderColor: 'black',
      borderWidth: 1,
    },
    button: {
      margin: 10
    }
});

export default TodoInput;