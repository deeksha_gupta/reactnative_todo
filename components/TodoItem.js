import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const TodoItem = props => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listItem}>
            <Text>{props.title}</Text>
        </View>
        <View style={styles.button}>
          <Button title="Delete" onPress={() => props.onDelete(props.id)}/>
        </View>  
      </View>
    );
};

const styles = StyleSheet.create({
  listContainer: {
    flexDirection: 'row'
  },
  listItem: {
    flex: 2,
    padding: 10,
    margin: 10,
    backgroundColor: '#ccc',
    borderColor: 'black',
    borderWidth: 1
  },
  button: {
    margin: 10
  }
});
export default TodoItem;